var express = require('express');
var router = express.Router();
const {getName} = require('../services/user.service')
const {saveName} = require('../services/user.service')
const {isAuthorized} = require('../middlewares/auth.middleware')

router.get('/', function (req, res, next){
  res.send(`Welcome to the Jungle`);
})

router.post('/', isAuthorized, function(req, res, next) {
const result = saveName(req.body);
if (result) {
  res.send(`Your admin is ${getName(req.body)}`);
} else {
  res.status(400).send('Errreeere')
}
  });
module.exports = router;