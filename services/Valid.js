class Valid {constructor(model) {this.model = model;}
validateId(obj) {return !('id' in obj);}
validateExistFields(obj) {
for (let i in this.model) {
if (!(i in obj) && i !== 'id') return i;}
return true;}

validateEmptyFields(obj) {
for (let i in obj) {
if (!obj[i]) return i;}
return true;}

validateExcessFields(obj) {
for (let i in obj) {
if (!(i in this.model)) return i;}
return true;}

removeExcessFields(obj) {let validUser = {};
for (let i in obj) {   
if ((i in this.model) && i !== 'id') {
validUser[i] = obj[i];}}
return validUser;}}

exports.Valid = Valid;    