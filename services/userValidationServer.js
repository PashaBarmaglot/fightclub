const {user} = require('../models/user');
const {Valid} = require('../services/Valid');

class UserValidationService extends valid {
constructor() {super(user);}

validateEmail(email) {
var r = /\S+@\S+\.\S+/;
return r.test(email);}

validatePhoneNumber(phoneNumber) {
let r = /\+\d{12}/;
return r.test(phoneNumber);}

validatePassword(password) {
let r = /.{3}/;
return r.test(password);
}
}

exports.UserValidationService = new UserValidationService();